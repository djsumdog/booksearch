FROM python:3.7.2

WORKDIR /usr/src/app

ENV G_API_KEY setyourapikey
ENV PAGE_SIZE 10

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY booksearch /usr/src/app/booksearch
CMD python -m booksearch.service
