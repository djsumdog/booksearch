# BookSeach

This is one of those pre-interview assignments.

# Running via Docker

```
docker build -t booksearch .
docker run -d -p 5000:5000 --rm --name booksearch \
    -e G_API_KEY=<insert Google API Key> \
    -e PAGE_SIZE=<integer between 1 and 40, 10 default>
docker logs -f booksearch
```

The application should now be accessible at http://localhost:5000

# Testing

You'll need BeautifulSoup4 for testing (and pytest is recommended).

```
pip3 install --user bs4
pip3 install --user flask
pytest -v
```

# Design Notes

I decided to use Python and Flask because I wanted to meet the design requirements completely, but also as fast as possible. I'm familiar with Python, Flask and pytest, which let me create this application quickly as well as write useful unit tests. It's a minimalist application which accomplishes its goal with absolutely no Javascript, or complex frameworks and is cross browser compatible.

# Known Issues

Google Books API returns a 400 on some paginated requests, even if a previous requests indicates enough results are returned that the page should exist. This probably has to do with the way Google estimates search results.

# LICENSE

GNU Affero General Public License v3
