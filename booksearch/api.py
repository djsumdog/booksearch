import requests


class BooksAPI(object):

    def __init__(self, api_key):
        self.api_key = api_key

    def search(self, query, page=0, page_size=10):
        return requests.get('https://www.googleapis.com/books/v1/volumes',
                            params={'q': query, 'key': self.api_key,
                                    'startIndex': page * page_size,
                                    'maxResults': page_size})
