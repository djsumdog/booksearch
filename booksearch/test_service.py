from booksearch.service import service
import unittest
from flask import request
from bs4 import BeautifulSoup


class MockResult(object):

    def __init__(self, status_code, data):
        self.status_code = status_code
        self.data = data

    def json(self):
        return self.data


class MockBooksAPI(object):

    def __init__(self):
        self.test_items = [
            {
                'volumeInfo': {
                    'authors': ['Foo Buckman', 'Sally Rally'],
                    'imageLinks': {'smallThumbnail': 'http://example.com/can-smallthumb.jpg',
                                   'thumbnail': 'http://example.com/can-thumb.jpg'},
                    'publisher': 'An Actual Real Penguin from Cincinnati',
                    'title': 'The Canadian Threat'}},
            {
                'volumeInfo': {
                    'authors': ['Will Barrel', 'Sarah Billverman'],
                    'imageLinks': {'smallThumbnail': 'http://example.com/red-smallthumb.jpg',
                                   'thumbnail': 'http://example.com/red-thumb.jpg'},
                    'publisher': 'A Trailer Park in Seattle',
                    'title': 'Canadians and the Red Threat'}},
            {
                'volumeInfo': {
                    'authors': ['Biggie Smalls', 'Tallie Fallie'],
                    'imageLinks': {'smallThumbnail': 'http://example.com/bo-smallthumb.jpg',
                                   'thumbnail': 'http://example.com/bo-thumb.jpg'},
                    'publisher': 'BenderPress',
                    'title': 'The BoJack Horseman Show Staring Bojack Horseman'}}
        ]

    def search(self, query, start_idx=0, page_size=1):
        if query == 'one-no-pages':
            return MockResult(200, {'totalItems': 1, 'items': [self.test_items[0]]})
        if query == 'two-pages':
            return MockResult(200, {'totalItems': 3, 'items': [self.test_items[start_idx]]})
        if query == 'not-found':
            return MockResult(200, {'totalItems': 0})
        if query == '400error':
            return MockResult(400, {})


class ServiceTest(unittest.TestCase):

    def setUp(self):
        self.app = service(MockBooksAPI(), 2)

    def test_landing_page(self):
        with self.app.test_client() as client:
            resp = client.get('/')
            soup = BeautifulSoup(resp.data, 'html.parser')
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(soup.title.string, 'Search for book')
            self.assertEqual(len(soup.select("#searchresults")), 0)

    def test_one_result(self):
        with self.app.test_client() as client:
            resp = client.get('/search?query=one-no-pages')
            soup = BeautifulSoup(resp.data, 'html.parser')
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(soup.title.string, 'Search results for one-no-pages')
            self.assertEqual(len(soup.select("#searchresults")), 1)
            self.assertEqual(len(soup.select("#morepages")), 0)
            self.assertEqual(len(soup.select("#notfound")), 0)
            self.assertFalse('Next' in str(resp.data))
            self.assertFalse('Previous' in str(resp.data))

    def test_one_with_two_pages(self):
        with self.app.test_client() as client:
            resp = client.get('/search?query=two-pages')
            soup = BeautifulSoup(resp.data, 'html.parser')
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(soup.title.string, 'Search results for two-pages')
            self.assertEqual(len(soup.select("#searchresults")), 1)
            self.assertEqual(len(soup.select("#morepages")), 1)
            self.assertEqual(len(soup.select("#notfound")), 0)
            self.assertTrue('Next' in str(resp.data))
            self.assertFalse('Previous' in str(resp.data))

    def test_one_with_two_pages_second_page(self):
        with self.app.test_client() as client:
            resp = client.get('/search?query=two-pages&page=2')
            soup = BeautifulSoup(resp.data, 'html.parser')
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(soup.title.string, 'Search results for two-pages')
            self.assertEqual(len(soup.select("#searchresults")), 1)
            self.assertEqual(len(soup.select("#morepages")), 1)
            self.assertEqual(len(soup.select("#notfound")), 0)
            self.assertFalse('Next' in str(resp.data))
            self.assertTrue('Previous' in str(resp.data))

    def test_not_found(self):
        with self.app.test_client() as client:
            resp = client.get('/search?query=not-found')
            soup = BeautifulSoup(resp.data, 'html.parser')
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(soup.title.string, 'Search results for not-found')
            self.assertEqual(soup.select('#notfound')[0].string, "No books found for not-found")
            self.assertEqual(len(soup.select("#searchresults")), 0)
            self.assertEqual(len(soup.select("#morepages")), 0)
            self.assertFalse('Next' in str(resp.data))
            self.assertFalse('Previous' in str(resp.data))

    def test_400_error(self):
        with self.app.test_client() as client:
            resp = client.get('/search?query=400error')
            soup = BeautifulSoup(resp.data, 'html.parser')
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(soup.title.string, 'Error')
            self.assertEqual(len(soup.select("#notfound")), 0)
            self.assertEqual(len(soup.select("#searchresults")), 0)
            self.assertEqual(len(soup.select("#morepages")), 0)
            self.assertEqual(len(soup.select("#errorlist")), 1)
            self.assertEqual(soup.select('li')[0].string, "Bad Request. Have you configured a valid API key?")
            self.assertFalse('Next' in str(resp.data))
            self.assertFalse('Previous' in str(resp.data))
