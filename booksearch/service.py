from flask import Flask
from flask import render_template
from flask import request
import math
from booksearch.api import BooksAPI
import os
import sys


def service(api_client, page_size):

    app = Flask(__name__)

    @app.route('/')
    def index():
        return render_template('default.html', title='Search for book')

    @app.route('/search', methods=['GET'])
    def search():
        query = request.args.get('query', '')
        page = int(request.args.get('page', 1))
        api_results = api_client.search(query, page, page_size)
        errors = []

        if api_results.status_code == 400:
            errors.append('Bad Request. Have you configured a valid API key?')

        if len(errors) > 0:
            return render_template('default.html', title='Error',
                                   errors=errors)

        api_json = api_results.json()
        results = api_json['items'] if 'items' in api_json else []
        total_items = api_json['totalItems']
        total_pages = math.ceil(total_items / page_size)

        return render_template('default.html',
                               title=f'Search results for {query}',
                               results=results, query=query, errors=errors,
                               currentpage=page, totalpages=total_pages)

    return app


def main():

    if 'G_API_KEY' not in os.environ:
        sys.stderr.write('env variable G_API_KEY is not set\n')
        sys.exit(1)

    page_size = int(os.environ['PAGE_SIZE']) if 'PAGE_SIZE' in os.environ else 10
    client = BooksAPI(os.environ['G_API_KEY'])

    app = service(client, page_size)
    app.run(host='0.0.0.0')


if __name__ == '__main__':
    main()
